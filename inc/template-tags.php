<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Pidie_X
 */

if ( ! function_exists( 'pidie_x_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function pidie_x_posted_on() {
		$time_string = '<i class="fa fa-calendar"></i> <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<i class="fa fa-calendar"></i> <time class="entry-date published" datetime="%1$s">%2$s</time> <i class="fa fa-calendar-o"></i> <time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( '%s', 'post date', 'pidie-x' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'pidie_x_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function pidie_x_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( '%s', 'post author', 'pidie-x' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> <i class="fa fa-user-circle-o"></i> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'pidie_x_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function pidie_x_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'pidie-x' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links"> <i class="fa fa-bookmark"></i> ' . esc_html__( '%1$s', 'pidie-x' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'pidie-x' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links"> <i class="fa fa-tags"></i> ' . esc_html__( '%1$s', 'pidie-x' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link"> <i class="fa fa-comment"></i> ';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( ' <span class="screen-reader-text"> on %s</span>', 'pidie-x' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( ' <span class="screen-reader-text">%s</span>', 'pidie-x' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link"> <i class="fa fa-pencil-square-o"></i> ',
			'</span>'
		);
	}
endif;

if(!function_exists('pidie_x_single_thumbnail')):
	function pidie_x_single_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>
			<div class="pd-single-featured-thumbnail">
				<?php the_post_thumbnail('full', array('class'=>'pd-single-featured-image')) ?>
			</div>
			<?php
		endif;
	}
endif;

if(!function_exists('pidie_x_og_post')):
	function pidie_x_og_post($og = null){
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}
		if($og == 'title'){
			(is_singular()) ? the_title() : bloginfo('name') ;
		} elseif ($og == 'description') {
			bloginfo('description');
		} elseif ($og == 'image') {
			if (is_singular()) {
				echo wp_strip_all_tags(get_the_post_thumbnail_url(get_the_ID()));
			} else{
				echo '';
			}
		} else{
			(is_singular()) ? the_permalink() : bloginfo('url') ;
		}
	}
endif;

if ( ! function_exists( 'pidie_x_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function pidie_x_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<div class="post-thumbnail">
				<?php the_post_thumbnail(); ?>
			</div><!-- .post-thumbnail -->

		<?php else : ?>

		<div class="post-thumb">
				<?php
				if ( 'post' === get_post_type() ) {
					/* translators: used between list items, there is a space after the comma */
					$categories_list = get_the_category_list( esc_html__( ' ', 'pidie-x' ) );
					if ( $categories_list ) {
						/* translators: 1: list of categories. */
						printf( '<span class="cat-links"> ' . esc_html__( '%1$s', 'pidie-x' ) . '</span>', $categories_list ); // WPCS: XSS OK.
					}

					/* translators: used between list items, there is a space after the comma */
					$tags_list = get_the_tag_list( '', esc_html_x( ' ', 'list item separator', 'pidie-x' ) );
					if ( $tags_list ) {
						/* translators: 1: list of tags. */
						printf( '<span class="tags-links"> ' . esc_html__( '%1$s', 'pidie-x' ) . '</span>', $tags_list ); // WPCS: XSS OK.
					}
				}
				?>
				<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
					<?php
					the_post_thumbnail( 'post-thumbnail', array(
						'alt' => the_title_attribute( array(
							'echo' => false,
						) ),
					) );
					?>
				</a>
		</div>

		<?php
		endif; // End is_singular().
	}
endif;
